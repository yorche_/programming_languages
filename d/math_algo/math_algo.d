import std.stdio;

/++
 * @author   Jorge Eduardo Ascencio Esíndola
 * @version  v1.0
 ++/

/**
 * Fibonacci sequence
 *
 * @param n  positive integer
 * @return  fibonacci number associated
 **/
int fibonacci(int n){
  if(n < 0) return -1;
  if(n == 0 || n ==1) return 1;
  return fibonacci(n-1) + fibonacci(n-2);
}

/**
 * Factorial operation
 *
 * @param n  positive integer
 * @return  factorial number associated
 **/
int factorial(int n){
  if(n < 0) return -1;
  if(n == 0 || n == 1) return 1;
  return n * factorial(n-1);
}

/**
 * Leap-year
 *
 * @param x  year greater than 1582
 * @return  true if x is a leap-year
 **/
bool leap_year(int x){
  if(x < 1582) return false;
  if(x%400 == 0) return true;
  if(x%4 == 0 && x%100 != 0) return true;
  return false;
}

/**
 * Collatz algorithm
 *
 * @param x  positive integer
 **/
int collatz(int x){
  if(x == 1){
    writeln(" ", 1);
    return 1;
  }
  else{
    write(" ", x);
    if(x%2 == 0){
      return collatz(x / 2);
    } else {
      return collatz(x * 3 + 1);
    }
  }
  return 0;
}

/**
 * Greatest Common Divisor
 *
 * @param a  positive integer
 * @param b  positive integer
 * @return  euclidean algorithm result
 **/
int gcd(int a, int b){
  if(a < 1 || b < 1) return -1;
  if(a > b){ return euclidean(a, b); }
  else{ return euclidean(b, a); }
}

/**
 * Euclidean algorithm
 *
 * @param a  positive integer
 * @param b  positive integer
 * @return  greatest common divisor of a and b
 **/
int euclidean(int a, int  b){
  if(b == 1) return 1;
  int c = a / b;
  int d = a - b*c;
  if(d == 0){ return b; }
  else{ return euclidean(b, d); }
}

/**
 * Fibonacci iterative caller
 *
 * @param n  positive integer
 * @return  fibonacci iterative result
 **/
int fibonaccic(int n){
  if(n < 0) return -1;
  return fibonacci_car(1, 0, n);
}

/**
 * Fibonacci iterative withs carrying
 *
 * @param fib1  fibonacci n-1 carrying
 * @param fib2  fibonacci n-2 carrying
 * @param count  count down from n to 0
 * @return  fibonacci number associated
 **/
int fibonacci_car(int fib1, int fib2, int count){
  if(count == 0) return fib1;
  return fibonacci_car(fib1 + fib2, fib1, count-1);
}

/**
 * Factorial iterative caller
 *
 * @param n  positive integer
 * @return  factorial iterative result
 **/
int factorialc(int n){
  if(n < 0) return -1;
  return factorial_car(n-1, n);
}

/**
 * Factorial iterative with carrying
 *
 * @param count  count down from n to 0
 * @param x  factorial carrying
 * @return  factorial number associated
 **/
int factorial_car(int count, int x){
  if(count == 0) return x;
  return factorial_car(count-1, x*count);
}

/**
 * Least Common Multiple
 *
 * @param a  positive integer
 * @param b  positive integer
 * @return  least common multiple of a and b
 **/
int lcm(int a, int b){
  if(a < 0 || b < 0) return -1;
  return (a * b) / gcd(a, b);
}

void main(){
  writeln("Fibonacci(1) = ", fibonacci(1));
  writeln("Fibonacci(3) = ", fibonacci(3));
  writeln("Fibonacci(5) = ", fibonacci(5));
  writeln("Factorial(0) = ", factorial(0));
  writeln("Factorial(2) = ", factorial(2));
  writeln("Factorial(4) = ", factorial(4));
  writeln("bisiesto(1900) = ", leap_year(1900));
  writeln("bisiesto(2000) = ", leap_year(2000));
  writeln("bisiesto(2004) = ", leap_year(2004));
  write("collatz(3) : ");
  collatz(3);
  write("collatz(4) : ");
  collatz(4);
  writeln("mcd(5,10) = ", gcd(5, 10));
  writeln("mcd(6,5) = ", gcd(6, 5));
  writeln("mcd(4,6) = ", gcd(4, 6));
  writeln("fibonacci(0) = ", fibonaccic(0));
  writeln("fibonacci(2) = ", fibonaccic(2));
  writeln("fibonacci(4) = ", fibonaccic(4));
  writeln("factorial(1) = ", factorialc(1));
  writeln("factorial(3) = ", factorialc(3));
  writeln("factorial(5) = ", factorialc(5));
  writeln("mcm(5,10) = ", lcm(5, 10));
  writeln("mcm(4,6) = ", lcm(4, 6));
}