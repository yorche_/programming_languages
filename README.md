# Programming languages learned

## Documentación del archivo
@author  autor del archivo

@version versión del archivo

la especificación de cada programa se encuentra en la carpeta 'notes'

## Documentación de funciones/métodos
Descripción de la función/método

@param(s)     Describe un parametro solicitado por la función/método

@return(s)    Describe al valor devuelto - no aplica si regresa el tipo void -

¡Todas las variables deben estar comentadas especificando su razón de ser!


# Estructura, en orden, de las carpetas de cada lenguaje de programación

* <dl> <dt>Nombre del lenguaje de programación</dt> </dl>
    * <dl> <dt>helloworld</dt> </dl>

        Primer programa - Hello world

    * <dl> <dt>helloconsole</dt> </dl>

        Primer interacción con el usuario, el programa le pregunta su nombre y lo saluda

    * <dl> <dt>cadenas</dt> </dl>
    /Nombre en español para evitar confusión con el tipo o clase String/
        
        Ejemplos de algoritmos sobre cadenas. Definición de cada algoritmo en notes/str_algo/string_algorithms
    
    * <dl> <dt>math_algo</dt> </dl>
        
        Algoritmos matemáticos. Definición de cada algoritmo en notes/math_algo/matemathic_algorithms
        