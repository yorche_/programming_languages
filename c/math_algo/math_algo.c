#include <stdio.h>

/*
 * Fibonacci function
 *
 * @param n  positive integer
 * return fiboncacci number associated to n
 */
int fibonacci(int n){
  if(n < 0){ return -1; }
  if(n == 0){ return 1; }
  if(n == 1){ return 1; }
  return fibonacci(n-1) + fibonacci(n-2);
}

/*
 * Factorial function
 *
 * @param n  positive integer
 * return fatorial number associated to n
 */
int factorial(int n){
  if(n < 0){ return -1; }
  if(n == 0 || n == 1){ return 1; }
  return n * factorial(n-1);
}

/*
 * Evaluate if a given year is leap-year
 *
 * @param x  year to evaluate
 * return 1 is leap-year, 0 otherwise
 */
int leap_year(int x){
  if(x < 1582){ return 0; }
  if(x%400 == 0){ return 1; }
  if(x%4 == 0 && x%100 != 0){ return 0; }
}

/*
 * Collatz algorithm
 */
int collatz(int x){
  if(x == 1){
    printf("1\n");
    return 1;
  }
  printf("%d ", x);
  if(x%2 == 0){
    collatz(x/2);
  } else {
    collatz(x*3+1);
  }
  return 0;
}

/*
 * Euclides algorithm
 *
 * @params a, b  positive integers
 * return greatest common divisor
 */
int euclides(int a, int b){
  if(b == 1){ return 1; }
  int c = a / b;
  int d = a - b*c;
  if(d == 0){ return b; }
  return euclides(b, d);
}

/*
 * Call euclides function to calculate Greatest Common Divisor of two numbers
 * @params a, b  two integers
 */
int gcd(int a, int b){
  if(a < 0 || b < 0){ return 0; }
  if(a > b){ return euclides(a, b); }
  return euclides(b, a);
}

/*
 * Calculate Least Common Multiple of two number
 * @param a, b  two integers
 */
int lcm(int a, int b){
  if(a < 0 || b < 0){ return 0; }
  return (a*b) / gcd(a, b);
}

/*
 * Auxiliar fibonacci function with carrying
 *
 * @param fib1  fibonacci(count-1)
 * @param fib2  fibonacci(count-2)
 * @param count  countdown from n to 0
 */
int fibonacci_car(int fib1, int fib2, int count){
  if(count == 0) return fib1;
  return fibonacci_car(fib1+fib2, fib1, count-1);
}

/*
 * Fibonacci iterative
 *
 * @param n  positive integer
 * return fibonacci number associated
 */
int fibonaccic(int n){
  if(n < 0){ return -1; }
  return fibonacci_car(1, 0, n);
}

/*
 * Auxiliar factorial function iterative
 * 
 * @param count  countdown from n to 0
 * @param car  carrying
 */
int factorial_car(int count, int car){
  if(count == 0){ return car; }
  return factorial_car(count-1, count*car);
}

/*
 * Factorial iterative
 *
 * @param n  positive integer
 * return factorial number associated
 */
int factorialc(int n){
  if(n < 0){ return -1; }
  return factorial_car(n-1, n);
}

int main(){
  printf("fibonacci(1) = %d\n", fibonacci(1));
  printf("fibonacci(3) = %d\n", fibonacci(3));
  printf("fibonacci(5) = %d\n", fibonacci(5));
  printf("factorial(0) = %d\n", factorial(0));
  printf("factorial(2) = %d\n", factorial(2));
  printf("factorial(4) = %d\n", factorial(4));
  printf("bisiesto 2000 : %d\n", leap_year(2000));
  printf("bisiesto 1900 : %d\n", leap_year(1900));
  collatz(2);
  collatz(3);
  collatz(5);
  printf("mcd(5,10) = %d\n", gcd(5, 10));
  printf("mcd(6,5) = %d\n", gcd(6, 5));
  printf("mcd(4,10) = %d\n", gcd(4, 10));
  printf("mcm(6,4) = %d\n", lcm(6, 4));
  printf("mcm(4,5) = %d\n", lcm(4,5));
  printf("mcm(5,10) = %d\n", lcm(5, 10));
  printf("fibonacci_carring(0) = %d\n", fibonaccic(1));
  printf("fibonacci_carring(2) = %d\n", fibonaccic(2));
  printf("fibonacci_carring(4) = %d\n", fibonaccic(4));
  printf("factorial_carring(1) = %d\n", factorialc(1));
  printf("factorial_carring(3) = %d\n", factorialc(3));
  printf("factorial_carring(5) = %d\n", factorialc(5));
  return 1;
}
