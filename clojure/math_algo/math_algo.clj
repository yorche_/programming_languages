;; Fibonacci function
;; @param n  positive integer
;; @returns fibonacci number associated to n
(defn fibonacci [n]
  (cond
    (< n 0) -1
    (= n 0) 1
    (= n 1) 1
    :else (+ (fibonacci (- n 1)) (fibonacci (- n 2)))
  )
)

;; Factorial function
;; @param n  positive integer
;; @returns factorial number associated to n
(defn factorial [n]
  (cond
    (< n 0) -1
    (= n 0) 1
    :else (* (factorial (- n 1)) n)
  )
)

;; Leap-year function
;; Evaluate if a given year is leap-year
;; @param x  year greater than 1582
;; @returns true if is leap, false otherwise
(defn leap-year [x]
  (cond
    (< x 1582) false
    (= (mod x 400) 0) true
    (and (= (mod x 4) 0) (= (mod x 100) 0)) false
    :else true
  )
)

;; Collatz algorithm
(defn collatz [x]
  (print x " ")
  (cond
    (< x 1) -1
    (= x 1) 1
    (= (mod x 2) 0) (collatz (/ x 2))
    :else (collatz (+ (* x 3) 1))
  )
)

;; Euclides algorithm
;; @param a  positive integer
;; @param b  positive integer
;; @returns greatest common divisor
(defn euclides [a b]
  (def c (quot a b))
  (def d (- a (* b c)))
  (cond
    (= b 1) 1
    (= d 0) b
    :else (euclides b d)
  )
)

;; Greatest common divisor
;; call euclides algorithm
;; @param a  positive integer
;; @param b  positive integer
(defn gcd [a b]
  (cond
    (> a b) (euclides a b)
    :else (euclides b a)
  )
)

;; Fibonacci iterative function
;; @param fib1  fibonacci n-1 carrying
;; @param fib2  fibonacci n-2 carrying
;; @param count  counter from n to 0
;; @returns fibonacci number carried
(defn fibonacci_car [fib1 fib2 count]
  (cond
    (= count 0) fib1
    :else (fibonacci_car (+ fib1 fib2) fib1 (- count 1))
  )
)

;; Fibonacci iterative caller
;; @param n  number to search
(defn fibonaccic [n]
  (cond
    (> n -1) (fibonacci_car 1 0 n)
    :else -1
  )
)

;; Factorial iterative function
;; @param count  counter from n to 0
;; @param ca  factorial carrying
;; @returns factorial number carried
(defn factorial_car [count ca]
  (cond
    (= count 0) ca
    :else (factorial_car (- count 1) (* ca count))
  )
)

;; Factorial iterative caller
;; @param n  number to search
(defn factorialc [n]
  (cond
    (= n 0) 1
    (> n 0) (factorial_car (- n 1) n)
    :else -1
  )
)

;; Least common multiple
;; @param a  positive integer
;; @param b  positive integer
;; @returns  least common multiple between a and b
(defn lcm [a b]
  (cond
    (> a b) (/ (* a b) (gcd a b))
    :else (/ (* a b) (gcd b a))
  )
)

(println "fibonacci(1) = " (fibonacci 1))
(println "fibonacci(3) = " (fibonacci 3))
(println "fibonacci(5) = " (fibonacci 5))
(println "factorial(0) = " (factorial 0))
(println "factorial(2) = " (factorial 2))
(println "factorial(4) = " (factorial 4))
(println "bisiesto 2000? : " (leap-year 2000))
(println "bisiesto 1900? : " (leap-year 1900))
(println "bisiesto 2004? : " (leap-year 2004))
(collatz 3)
(println "")
(collatz 4)
(println)
(println "mcd(5,10) = " (gcd 5 10))
(println "mcd(6,5) = " (gcd 6 5))
(println "mcd(4,10) = " (gcd 4 10))
(println "fibonacci(0) = " (fibonaccic 0))
(println "fibonacci(2) = " (fibonaccic 2))
(println "fibonacci(4) = " (fibonaccic 4))
(println "factorial(1) = " (factorialc 1))
(println "factorial(3) = " (factorialc 3))
(println "factorial(5) = " (factorialc 5))
(println "mcm(6,4) = " (lcm 6 4))
(println "mcm(5,4) = " (lcm 5 4))
(println "mcm(5,10) = " (lcm 5 10))