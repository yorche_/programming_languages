/**
* @return un saludo
*/
fun hola() : String {
  return "Hola"
}


/**
* Verifica si una palabra es palindromo
*
* @param word  la cadena a evaluar
* @return true si lo es, false en otro caso
*/
fun palindromo(word : String) : Boolean {
  var i = 0;
  var l = word.length - 1;
  while(i < l){
    if(word[i++] != word[l--]) return false;
  }
  return true;
}


/**
* Cuenta el número de repeticiones de un caracter en una cadena
*
* @param word  la palabra a iterar
* @param ch  el caracter a buscar
* @return el número de apariciones de ch en word
*/
fun repeticiones(word : String, ch : Char) : Int {
  var c = 0;
  for(i in 0..(word.length - 1)){
    if(word[i] == ch) c++;
  }
  return c;
}

/**
* Invierte una palabra
*
* @param word  la palabra a invertir
* @return la cadena invetida
*/
fun reversa(word : String) : String {
  return word.reversed();
}

fun main(args : Array<String>) {
  println(hola());
  print("Escribe una palabra   ");
  val word = readLine()!!;
  println("Es palindromo : "+ palindromo(word));
  print("Caracter a buscar:   ");
  val ch = readLine()!!;
  println("repeticiones : "+ repeticiones(word, ch[0]));
  println("invertida : "+ reversa(word));
}