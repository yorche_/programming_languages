# Fibonacci recursive
#
# param *n*  positive integer
# Returns  fibonacci number asociated to n
def fibonacci(n)
  if n < 0
    return -1
  end
  if n == 0 || n == 1
    return 1
  end
  return fibonacci(n-1) + fibonacci(n-2)
end

# Factorial recursive
#
# param *n*  positive integer
# Returns  factorial number asociated
def factorial(n)
  if n < 0
    return -1
  end
  if n == 0
    return 1
  end
  return n * factorial(n-1)
end

# Verify if a given year is leap year
#
# param *x*  year to evaluate
# Returns  true if is leap year, false otherwise
def leap_year(x)
  if x < 1582
    return false
  end
  if x%400 == 0 || (x%4 == 0 && x%100 != 0)
    return true
  end
  return false
end

# Collatz algorithm
def collatz(x)
  if x == 1
    print "1\n"
    return 1
  else
    print "#{x} "
  end
  if x%2 == 0
    return collatz(x / 2)
  else
    return collatz(x * 3 + 1)
  end
end

# Greates Common Divisor
# 
# param *a*  positive integer
# param *b*  positive integer
# Returns  euclides algorithm result
def gcd(a, b)
  if a < 1 || b < 1
    return -1
  else
    return a > b ? euclidean(a, b) : euclidean(b, a)
  end
end

# Euclidean algorithm
#
# param *a*  positive integer
# param *b*  positive integer
# Returns  greatest common divisor
def euclidean(a, b)
  if b == 1
    return 1
  end
  c = a/b
  d = a - b * c
  return d == 0 ? b : euclidean(b, d)
end

# Fibonacci iterative caller
#
# param *n*  positive integer
# Returns  fibonacci number asociated
def fibonaccic(n)
  if n < 0
    return -1
  else
    return fibonacci_car(1, 0, n)
  end
end

# Fibonacci iterative with carrying
#
# param *fib1*  fibonacci(n-1)
# param *fib2*  fibonacci(n-2)
# param *count*  count down from n to 0
# Returns  fibonacci carried
def fibonacci_car(fib1, fib2, count)
  if count == 0
    return fib1
  else
    return fibonacci_car(fib1 + fib2, fib1, count-1)
  end
end

# Factorial iterative caller
#
# param *n*  positive integer
def factorialc(n)
  if n < 0
    return -1
  end
  return factorial_car(n-1, n)
end

# Factorial iterative with carrying
#
# param *c*  countdown from n to 0
# param *x*  carried number
def factorial_car(c, x)
  if c == 0
    return x
  end
  return factorial_car(c-1, x*c)
end

# Least Common Multiple
#
# param *a*  positive integer
# param *b*  positive integer
# Returns  least common multiple of a and b
def lcm(a, b)
  return (a * b) / gcd(a, b)
end

puts "fibonacci(1) = #{fibonacci(1)}"
puts "fibonacci(3) = #{fibonacci(3)}"
puts "fibonacci(5) = #{fibonacci(5)}"
puts "factorial(0) = #{factorial(0)}"
puts "factorial(2) = #{factorial(2)}"
puts "factorial(4) = #{factorial(4)}"
puts "bisiesto(1900) = #{leap_year(1900)}"
puts "bisiesto(2000) = #{leap_year(2000)}"
puts "bisiesto(2004) = #{leap_year(2004)}"
puts "collatz(3):"
collatz(3)
puts "collatz(4):"
collatz(4)
puts "mcd(5,10) = #{gcd(5, 10)}"
puts "mcd(6,5) = #{gcd(6, 5)}"
puts "mcd(4,6) = #{gcd(4, 6)}"
puts "fibonacci(0) = #{fibonaccic(0)}"
puts "fibonacci(2) = #{fibonaccic(2)}"
puts "fibonacci(4) = #{fibonaccic(4)}"
puts "factorial(1) = #{factorialc(1)}"
puts "factorial(3) = #{factorialc(3)}"
puts "factorial(5) = #{factorialc(5)}"
puts "mcm(10,5) = #{lcm(10, 5)}"
puts "mcm(6,4) = #{lcm(6,4)}"