defmodule MathAlgo do
  @moduledoc """
  Módulo de algoritmos con números

    - author:   Jorge Eduardo Ascencio Espíndola
    - version:  v1.0
  """

  @doc """
  Fibonacci function

  ## Parameters

  - n: positive number
  
  ## Return 
     
     Fibonacci number associated
  """
  @spec fibonacci(Integer.t()) :: Integer.t()
  def fibonacci(n) do
    cond do
      n < 0 -> -1
      n == 0 -> 1
      n == 1 -> 1
      true -> (fibonacci(n-1) + fibonacci(n-2))
    end
  end

  @doc """
  Factorial function

  ## Parameters

    - n: positive integer

  ## Return

     Factorial number associated
  """
  @spec factorial(Integer.t()) :: Integer.t()
  def factorial(n) do
    cond do
      n < 0 -> -1
      n == 0 -> 1
      true -> n * factorial(n-1)
    end
  end

  @doc """
  Verify if a year given is leap-year

  ## Parameters

    - x: year

  ## Return

     true is is a leap-year, false otherwise
  """
  @spec leap_year(Integer.t()) :: Boolean.t()
  def leap_year(x) do
    cond do
      x < 1582 -> false
      rem(x, 400) == 0 -> true
      true -> rem(x, 4) == 0 and rem(x, 100) != 0
    end
  end

  @doc """
  Collatz algotrithm

  ## Parameters

    - x: positive integer
  """
  @spec collatz(Integer.t()) :: Integer.t()
  def collatz(x) do
    IO.puts to_string(x)
    cond do
      x < 1 -> -1
      x == 1 -> 1
      rem(x, 2) == 0 -> collatz(div(x, 2))
      true -> collatz(x*3+1)
    end
  end

  @doc """
  Euclidean algorithm caller

  ## Parameters

    - x: positive integer
    - y: positive integer

  ## Return

     Euclidean greatest common divisor
  """
  @spec gcd(Integer.t(), Integer.t()) :: Integer.t()
  def gcd(x, y) do
    cond do
      x < 0 -> -1
      y < 0 -> -1
      x > y -> euclidean(x, y)
      true -> euclidean(y, x)
    end
  end

  @doc """
  Euclidean algorithm

  ## Parameters

    - a: positive integer
    - b: positive integer

  ## Return

     Greatest common divisor betwen x and y
  """
  @spec euclidean(Integer.t(), Integer.t()) :: Integer.t()
  def euclidean(a, b) do
    c = div(a, b)
    d = a - b*c
    cond do
      b == 1 -> 1
      d == 0 -> b
      true -> euclidean(b, d)
    end
  end

  @doc """
  Fibonacci carrying function

  ## Parameters

    - fib1: fibonacci n-1 carrying
    - fib2: fibonacci n-2 carrying
    - count: counter down from n to 0

  ## Return

     Fibonacci number carried
  """
  @spec fibonacci_car(Integer.t(), Integer.t(), Integer.t()) :: Integer.t()
  def fibonacci_car(fib1, fib2, count) do
    cond do
      count == 0 -> fib1
      true -> fibonacci_car(fib1+fib2, fib1, count-1)
    end
  end

  @doc """
  Fibonacci carrying caller

  ## Parameters

    - n: positive integer

  ## Return

     Fibonacci carrying number
  """
  @spec fibonaccic(Integer.t()) :: Integer.t()
  def fibonaccic(n) do
    cond do
      n < 0 -> -1
      true -> fibonacci_car(1, 0, n)
    end
  end

  @doc """
  Factorial carrying function

  ## Parameters

    - count: count down from n to 0
    - car: factorial carrying 

  ## Return

     Factorial number carried
  """
  @spec factorial_car(Integer.t(), Integer.t()) :: Integer.t()
  def factorial_car(count, car) do
    cond do
      count == 0 -> car
      true -> factorial_car(count-1, car*count)
    end
  end

  @doc """
  Factorial carrying caller

  ## Parameters

    - n: positive integer

  ## Return

     Factorial carrying number
  """
  @spec factorialc(Integer.t()) :: Integer.t()
  def factorialc(n) do
    cond do
      n < 0 -> -1
      true -> factorial_car(n-1, n)
    end
  end

  @doc """
  Least common multiple

  ## Parameters

    - a: positive integer
    - b: positive integer

  ## Return

     Least common multiple of a and b
  """
  @spec lcm(Integer.t(), Integer.t()) :: Integer.t()
  def lcm(a, b) do
    cond do
      a < 0 || b < 0 -> -1
      true -> div(a * b, gcd(a, b))
    end
  end
end

IO.puts "fibonacci(1) = " <> to_string(MathAlgo.fibonacci(1))
IO.puts "fibonacci(3) = " <> to_string(MathAlgo.fibonacci(3))
IO.puts "fibonacci(5) = " <> to_string(MathAlgo.fibonacci(5))
IO.puts "factorial(0) = " <> to_string(MathAlgo.factorial(0))
IO.puts "factorial(2) = " <> to_string(MathAlgo.factorial(2))
IO.puts "factorial(4) = " <> to_string(MathAlgo.factorial(4))
IO.puts "bisiesto(1900) = " <> to_string(MathAlgo.leap_year(1900))
IO.puts "bisiesto(2000) = " <> to_string(MathAlgo.leap_year(2000))
IO.puts "bisiesto(2004) = " <> to_string(MathAlgo.leap_year(2004))
IO.puts "collatz(3) = "
MathAlgo.collatz(3)
IO.puts "collatz(4) = "
MathAlgo.collatz(4)
IO.puts "mcd(5,10) = " <> to_string(MathAlgo.gcd(5, 10))
IO.puts "mcd(6,5) = " <> to_string(MathAlgo.gcd(6, 5))
IO.puts "mcd(4,6) = " <> to_string(MathAlgo.gcd(4, 6))
IO.puts "fibonacci(0) = " <> to_string(MathAlgo.fibonaccic(0))
IO.puts "fibonacci(2) = " <> to_string(MathAlgo.fibonaccic(2))
IO.puts "fibonacci(4) = " <> to_string(MathAlgo.fibonaccic(4))
IO.puts "factorial(1) = " <> to_string(MathAlgo.factorialc(1))
IO.puts "factorial(3) = " <> to_string(MathAlgo.factorialc(3))
IO.puts "factorial(5) = " <> to_string(MathAlgo.factorialc(5))
IO.puts "mcm(5,10) = " <> to_string(MathAlgo.lcm(5, 10))
IO.puts "mcm(4,6) = " <> to_string(MathAlgo.lcm(4, 6))