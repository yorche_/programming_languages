#include <iostream>
using namespace std;

/*
 * @autor  Jorge Eduardo Ascencio Espíndola
 * @version v1.0
 */

/*
 * Fibonacci
 * @param n  positive integer
 * @return  fibonacci number associated
 */
int fibonacci(int n){
  if(n == 0 || n == 1){ return 1; }
  return fibonacci(n-1) + fibonacci(n-2);
}

/*
 * Factorial
 * @param n  positive integer
 * @return  fibonacci number associated
 */
int factorial(int n){
  if(n == 0){ return 1; }
  return n * factorial(n-1);
}

/*
 * Leap-year function
 * @param x  integer greater than 1582
 * @return  true if is leap-year, false otherwise
 */
bool leap_year(int x){
  if(x < 1582) { return false; }
  if(x%400 == 0){ return true; }
  if(x%4 == 0 && x%100 != 0){ return true; }
  return false;
}

/*
 * Collatz algorithm
 * @param n  positive integer
 */
int collatz(int n){
  if(n < 1) { return -1; }
  cout << n << " ";
  if(n == 1){
    cout << "\n";
    return 0;
  }
  if(n%2 == 0) return collatz(n/2);
  return collatz(n*3+1);
}

/*
 * Euclides algorithm
 * @param a  positive integer
 * @param b  positive integer
 * @return greatest common divisor between a and b
 */
int euclides(int a, int b){
  if(b == 1){ return 1; }
  int c = a/b;
  int d = a - b*c;
  if(d == 0){ return b; }
  return euclides(b, d);
}

/*
 * Function calls euclides algorithm
 * @param a  positive integer
 * @param b  positive integer
 */
int gcd(int a, int b){
  if(a < 0 || b < 0){ return -1; }
  if(a > b){ return euclides(a, b); }
  return euclides(b, a);
}

/*
 * Fibonacci iterative version
 * @param fib1  fibonacci n-1 carrrying
 * @param fib2  fibonacci n-2 carrying
 * @param count  count down from n to 0
 * @return fibonacci carried
 */
int fibonacci_car(int fib1, int fib2, int count){
  if(count == 0){ return fib1; }
  return fibonacci_car(fib1 + fib2, fib1, count-1);
}

/*
 * Calls iterative fibonacci
 * @param n  positive integer
 * @return fibonacci number associated to n
 */
int fibonaccic(int n){
  if(n < 0){ return -1; }
  return fibonacci_car(1, 0, n);
}

/*
 * Factorial iterative version
 * @param car  factorial carried
 * @param count  count down from n to 0
 * @return factorial number carried
 */
int factorial_car(int car, int count){
  if(count == 0){ return car; }
  return factorial_car(car*count, count-1);
}

/*
 * Calls iterative factorial
 * @param n  positive integer
 * @return factorial number associated to n
 */
int factorialc(int n){
  if(n < 0){ return -1; }
  if(n == 0){ return 1; }
  return factorial_car(n, n-1);
}

/*
 * Least common multiple
 * @param a  positive integer
 * @param b  positive integer
 * @return  least common multiple between a and b 
 */
int lcm(int a, int b){
  if(a < 0 || b < 0){ return -1; }
  return (a*b)/gcd(a, b);
}

int main(){
  cout << "fibonacci(1) = " << fibonacci(1) << "\n";
  cout << "fibonacci(3) = " << fibonacci(3) << "\n";
  cout << "fibonacci(5) = " << fibonacci(5) << "\n";
  cout << "factorial(0) = " << factorial(0) << "\n";
  cout << "factorial(2) = " << factorial(2) << "\n";
  cout << "factorial(4) = " << factorial(4) << "\n";
  cout << "bisiesto(1900) : " << leap_year(1900) << "\n";
  cout << "bisiesto(2000) : " << leap_year(2000) << "\n";
  cout << "bisiesto(2004) : " << leap_year(2004) << "\n";
  cout << "collatz(3): ";
  collatz(3);
  cout << "collatz(4): ";
  collatz(4);
  cout << "mcd(5,10) = " << gcd(5, 10) << "\n";
  cout << "mcd(6,5) = " << gcd(6, 5) << "\n";
  cout << "mcd(4,10) = " << gcd(4, 10) << "\n";
  cout << "fibonacci(0) = " << fibonaccic(0) << "\n";
  cout << "fibonacci(2) = " << fibonaccic(2) << "\n";
  cout << "fibonacci(4) = " << fibonaccic(4) << "\n";
  cout << "factorial(1) = " << factorialc(1) << "\n";
  cout << "factorial(3) = " << factorialc(3) << "\n";
  cout << "factorial(5) = " << factorialc(5) << "\n";
  cout << "mcm(5,10) = " << lcm(5, 10) << "\n";
  cout << "mcm(6,4) = " << lcm(6, 4) << "\n";
}
