# Fibonacci
Fibonacci sequence defined as:

* Fibonacci(0) = 1
* Fibonacci(1) = 1
* Fibonacci(n) = Fibonacci(n-1) + Fibonacci(n-2), with n >= 0


## Recursive:
fibonacci(n):
    
    n == 0 or n == 1 -> return 1
    return fibonacci(n-1) + fibonacci(n-2)

## Iterative with carrying:
fibonaccic(n):
    
    x == 0 or x == 1 return 1;
    return fibonacci_car(1, 0, n)

fibonacci_car(fib1, fib2, count):

    count == 0 -> return fib1
    return fibonacci_car(fib1 + fib2, fib1, count-1)


# Factorial
Factorial operation defined as:
* Factorial(0) = 1
* Factorial(n) = n * Factorial(n-1), n >= 0

## Recursive:
factorial(n):

    n == 0 or n == 1 -> return 1
    return n * factorial(n-1)

## Iterative carrying:
factorialc(n):
    
    return factorial_car(n-1, n):

factorial_car(x, c):
    
    x == 0 -> return c
    return factorial_car(x-1, x*c)


# Leap-year
Is a calendar year containing one additional day.

* Leap-year(x) = x mod 400 == 0 or (x mod 4 == 0 and x mod 100 != 0) ? Yes : False, x > 1582


# Collatz
Collatz conjecture

Collatz(x):
* x = 1 ? Fin
* x par -> collatz(x/2)
* x impar -> collatz(x*3+1)


# Greatest Common Divisor
gcd(a, b):

    a > b ? -> euclides(a, b)
    : -> euclides(b, a)

# Euclidean
Algorithm computes the greatest common divisor (GCD) of two numbers

euclides(a, b):

    b == 1 ? return 1
    c = a / b
    d = a - b*c
    d == 0 ? -> return b
    : -> return euclides(b, d)

# Least Common Multiple
Compute least common multiple of two numbers from greatest common divisor (gcd)

lcm(a, b):

    return (a * b) / gcd(a, b)