## Invertir
Hace una copia inviertida de la cadena que se le pasa como parametro.

Regresa la nueva cadena.


## Palindromo

Verifica si la cadena pasada como parametro es palindromo o no.

Regresa sí si lo es, no en otro caso.


## Repeticiones

Calcula el número de apariciones de un caracter en una cadena.

Regresa el número de apariciones.


### Funciones auxiliares

#### limpia

Quita o cambia el último caracter de la cadena ingresada por el usuario.